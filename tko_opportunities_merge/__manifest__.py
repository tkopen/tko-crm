# © 2019 TKOpen <https://tkopen.com>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    'name': 'CRM Merge Opportunities',
    'summary': 'CRM Merge Opportunities',
    'description': 'CRM Merge Opportunities',
    'author': 'TKOpen',
    'category': 'crm',
    'license': 'LGPL-3',
    'website': 'https://tkopen.com',
    'version': '12.0.0.2',
    'support': 'info@tkopen.com',
    'sequence': 1,
    'depends': [
        'crm',
    ],
    'data': [
    ],
    'init_xml': [],
    'update_xml': [],
    'css': [],
    'demo_xml': [],
    'test': [],
    'external_dependencies': {
        'python': [
        ],
        'bin': [],
    },
    'images': ['static/description/banner.png'],
    'application': True,
    'installable': True,
    'auto_install': False,
}
