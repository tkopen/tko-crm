from odoo import models, fields, api
from odoo.exceptions import Warning
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)

class CrmLead(models.Model):
    _inherit = 'crm.lead'

    is_finished = fields.Boolean('Is Finished?')

    @api.model_create_multi
    def create(self, vals_list):
        leads = self.env['crm.lead']
        for vals in vals_list:
            if (not self.env.context.get('no_merge') and
                    ('partner_address_email' in vals or 'email_from' in vals)):

                partner_address_email = (vals.get('partner_address_email') or
                                         vals.get('email_from') or '').strip()

                if partner_address_email:
                    partner = self.env['res.partner'].search([
                        ('email', 'ilike', partner_address_email)
                    ])

                    opportunity = self.search([
                        '|',
                        ('partner_id', 'in', partner.ids),
                        ('email_from', 'ilike', partner_address_email),
                        ('is_finished', '=', False),
                    ], limit=1, order='create_date desc')

                    if opportunity:
                        # Update the existing opportunity
                        existing_description = opportunity.description or ''
                        body = ''.join(f'<li>{field} : {value}</li>' for field, value in vals.items())

                        opportunity.message_post(body=body)
                        print("Found Existing Opportunity ....", opportunity)
                        leads |= opportunity
                        continue

            # Create a new record if no match found
            leads |= super(CrmLead, self).create([vals])

        return leads
