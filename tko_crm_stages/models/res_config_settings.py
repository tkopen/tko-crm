from odoo import fields, models, api


class CrmConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'


    duplicate_fallback_stage_id = fields.Many2one('crm.stage', string='Fallback Stage',
                                                  help='Fallback to this stage if a new opportunity is found'
                                                       'with same contact details as existing one')
    followup_stage_id = fields.Many2one('crm.stage', string='Follow up Stage',
                                        help="This will increase counter of followup_attempts")
    followup_attempts = fields.Integer('Follow up Attempts',
                                       help='Opportunity moves to Lost if it reaches the given number of times in follow up')

    @api.model
    def get_values(self):
        res = super(CrmConfigSettings, self).get_values()
        params = self.env['ir.config_parameter'].sudo()
        res.update({
            'duplicate_fallback_stage_id': int(params.get_param('tko_crm_stages.duplicate_fallback_stage_id', False)),
            'followup_stage_id': int(params.get_param('tko_crm_stages.followup_stage_id', False)),
            'followup_attempts': int(params.get_param('tko_crm_stages.followup_attempts', 3)),
        })
        return res

    def set_values(self):
        res = super(CrmConfigSettings, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param("tko_crm_stages.duplicate_fallback_stage_id", self.duplicate_fallback_stage_id.id or '')
        self.env['ir.config_parameter'].sudo().set_param("tko_crm_stages.followup_stage_id", self.followup_stage_id.id or '')
        self.env['ir.config_parameter'].sudo().set_param("tko_crm_stages.followup_attempts", self.followup_attempts)
        return res