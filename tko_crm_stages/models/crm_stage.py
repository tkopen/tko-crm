from odoo import models, fields, api


class CrmCaseStage(models.Model):
    _inherit = 'crm.stage'

    trigger_days = fields.Integer('Stage Change After', default=0)
    previous_stage_id = fields.Many2one('crm.stage', 'Previous Stage ',
                                        help='If set, an automated email will be sent only if target stage originated from from previous stage set.')
    next_stage_id = fields.Many2one('crm.stage', 'Stage To ',
                                    help='If set, Stage change will be triggered by system after specified number of days or on the specified days + 1, if you set zero the stage wonuld not change.')

    enter_template_id = fields.Many2one('mail.template', 'Enter Template')
    exit_template_id = fields.Many2one('mail.template', 'Exit Template')
    stage_control = fields.Selection([('s', 'System'), ('m', 'Manual')], string='Stage Control', required=True,
                                     default='m')

    activity_stage_id = fields.Many2one('crm.stage', string='On Activity Day Move To')