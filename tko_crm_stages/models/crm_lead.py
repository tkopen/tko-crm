from odoo import models, fields, api
from odoo.exceptions import UserError
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class CrmStageChange(models.Model):
    _name = 'crm.stage.change'
    _description = 'Stage Change Datetime'
    _order = 'change_datetime desc'

    lead_id = fields.Many2one('crm.lead', 'Opportunity')
    source_stage_id = fields.Many2one('crm.stage', 'Stage Src')
    dest_stage_id = fields.Many2one('crm.stage', 'Stage Dst')
    change_datetime = fields.Datetime('Datetime', default=fields.Datetime.now)


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    stage_change_lines = fields.One2many('crm.stage.change', 'lead_id', 'Stage Update Date', readonly=True)
    activity_type_id = fields.Many2one('mail.activity.type', string='Activity Type')

    def create_stage_change_record(self, source_stage, dest_stage):
        if source_stage and dest_stage:
            record = self.env['crm.stage.change'].create({'lead_id': self.id,
                                                          'source_stage_id': source_stage.id,
                                                          'dest_stage_id': dest_stage.id,
                                                          })
            return record

    def write(self, vals):
        res = True
        for record in self:
            trigger_email = False
            exit_stage = record.stage_id
            if 'stage_id' in vals.keys() and exit_stage.exit_template_id:
                # Send an Email on exiting stage
                if not record.partner_id.email:
                    raise UserError("[%s]: Customer's email is mandatory to send an Email!"%record.id)
                exit_stage.exit_template_id.send_mail(record.id, force_send=True,
                                                      raise_exception=False, email_values={'partner_ids': [(6, 0, [
                                                                                               self.partner_id.id])]})
            res = super(CrmLead, self).write(vals)
            enter_stage = record.stage_id
            if 'stage_id' in vals.keys() and exit_stage != enter_stage:
                # stage changed
                record.create_stage_change_record(exit_stage, enter_stage)
                ## Send automated email on entering stage
                if enter_stage and enter_stage.enter_template_id:
                    if not enter_stage.previous_stage_id:
                        trigger_email = True
                    else:
                        if enter_stage.previous_stage_id and enter_stage.previous_stage_id == exit_stage:
                            trigger_email = True

            if trigger_email:
                if not record.partner_id.email:
                    raise UserError("[%s]: Customer's email is mandatory to send an Email!"%record.id)
                # Send an Email
                enter_stage.enter_template_id.send_mail(record.id, force_send=True,
                                                        raise_exception=False, email_values={'partner_ids': [(6, 0, [
                                                                                                 self.partner_id.id])]})
            else:
                print("Dont trigger Email")
        return res

    def _update_crm_stages(self):
        ## Search Stages we need to check
        stages = self.env['crm.stage'].search(
            [('stage_control', '=', 's'), ('next_stage_id', '!=', False), ('trigger_days', '>', 0)])
        for stage in stages:
            _logger.info("Checking opportunities for system moderation in stage : %s" % stage.name)
            opportunities = self.env['crm.lead'].search([('stage_id', '=', stage.id)])
            for opportunity in opportunities:
                last_stage_change = self.env['crm.stage.change'].search(
                    [('lead_id', '=', opportunity.id), ('dest_stage_id', '=', opportunity.stage_id.id)], limit=1)
                last_stage_change_date = last_stage_change.change_datetime
                if last_stage_change_date:
                    days_since_action = (datetime.now().date() - last_stage_change_date.date()).days
                    if days_since_action > stage.trigger_days:
                        if opportunity.partner_id.email:
                            opportunity.write({'stage_id': stage.next_stage_id.id})
                        else:
                            opportunity.message_post(
                                body=('Customer not set, Please set a customer.'),
                                message_type='comment',
                                subtype_xmlid='mail.mt_note')

        return True

    @api.model
    def cron_process_expried_activities(self):
        _logger.info("Cron processing Expired Actitivities in Opportunities !!")
        waiting_stages = self.env['crm.stage'].search([('activity_stage_id', '!=', False)])
        for opportunitiy in self.search([('stage_id', 'in', waiting_stages.ids)]):
            # activities_after_deadline = self.env['mail.activity'].search(
            #     [('res_model', '=', 'crm.lead'), ('res_id', '=', opportunitiy.id),
            #      ('date_deadline', '>', fields.Date().today())])
            activities_before_deadline = self.env['mail.activity'].search(
                [('res_model', '=', 'crm.lead'), ('res_id', '=', opportunitiy.id),
                 ('date_deadline', '<=', fields.Datetime().now().replace(hour=23, minute=59, second=59))])
            if activities_before_deadline:
                opportunitiy.write({'stage_id': opportunitiy.stage_id.activity_stage_id.id})
                _logger.info("Opportunity :%s Moved to %s" % (opportunitiy, opportunitiy.stage_id.name))
        return True
