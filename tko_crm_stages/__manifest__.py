# © 2019 TKOpen <https://tkopen.com>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    'name': 'CRM Auto Email',
    'summary': 'CRM Auto Email',
    'description': 'CRM Auto Email',
    'author': 'TKOpen',
    'category': 'crm',
    'license': 'LGPL-3',
    'website': 'https://tkopen.com',
    'version': '12.0.0.2',
    'support': 'info@tkopen.com',
    'sequence': 1,
    'depends': [
        'crm',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/res_config_settings_view.xml',
        'data/cron_view.xml',
        'views/crm_stage_view.xml',
        'views/crm_lead_view.xml',
    ],
    'init_xml': [],
    'update_xml': [],
    'css': [],
    'demo_xml': [],
    'test': [],
    'external_dependencies': {
        'python': [
        ],
        'bin': [],
    },
    'images': ['static/description/banner.png'],
    'application': True,
    'installable': True,
    'auto_install': False,
}
